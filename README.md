# HLS reLive
Save and replay all HLS variants of a stream. Replay for debugging purpose only, not suitable for broadcasting.

## Install
Install dependencies (--user for local user only)

    pip install --user -r requirements.txt

## Usage
Save the whole HLS Live stream and its variants in the current directory. Can also be a single variant playlist.

    python hlsrelive.py save http://example.foo/index.m3u8 60

To replay that content simulating a new live with a segments window of 20 sec

    python hlsrelive.py replay index.m3u8 20
or only a single variant

    python hlsrelive.py replay playlist_0.m3u8 20

Content will be replayed on

    http://localhost:8080/index.m3u8

or in case of single variant

    http://localhost:8080/playlist_0.m3u8

## Limitations

 - Expired segments are always available
 - Does not set DATETIME properly


