"""
 *****************************************************************************
 * hlsrelive.py: HLS Live replay tool
 *****************************************************************************
 * Copyright © 2021 VideoLabs SAS and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************
"""
import urllib
import time
import m3u8
import os
import threading
import sys
import copy
from http.server import SimpleHTTPRequestHandler
from http.server import HTTPServer
from http.server import ThreadingHTTPServer
from http.server import HTTPStatus

def filename_only(string):
    uri = urllib.parse.urlparse(string)
    return os.path.basename(uri.path)

class SegmentsServerHandler(SimpleHTTPRequestHandler):

    def windowed_playlist(self, pl, start, duration):
        restr = copy.deepcopy(pl)
        restr.playlist_type = ''
        starttime = 0
        endtime = start + duration
        restr.segments.clear()
        media_sequence = restr.media_sequence
        if restr.discontinuity_sequence is None:
            discontinuity_sequence = 0
        else:
            discontinuity_sequence = restr.discontinuity_sequence
        for seg in pl.segments:
            segstart = starttime
            segend = segstart + seg.duration
            if segstart >= start and segstart < endtime:
                if len(restr.segments) == 0:
                    restr.media_sequence = media_sequence
                    restr.discontinuity_sequence = discontinuity_sequence
                    seg.program_date_time = seg.current_program_date_time
                restr.segments.append(seg)
            starttime = segend
            media_sequence += 1
            if seg.discontinuity:
                discontinuity_sequence += 1
        if not restr.segments or (restr.media_sequence + len(restr.segments)) == (pl.media_sequence + len(pl.segments)):
            restr.is_endlist = 1
        else:
            restr.is_endlist = 0
        return restr
        
    def do_GET(self):
        path = os.path.basename(self.path)
        if path in self.server.playlists:
            pl = self.windowed_playlist(self.server.playlists[path],
                                        time.time() - self.server.starttime,
                                        self.server.window)
            data = pl.dumps().encode("utf-8")
            self.send_response(HTTPStatus.OK)
            self.send_header("Content-type", "application/vnd.apple.mpegurl")
            self.send_header("Content-Length", str(len(data)))
            self.send_header("Last-Modified", self.date_time_string(time.time()))
            self.end_headers()
            self.wfile.write(data)
        else:
            SimpleHTTPRequestHandler.do_GET(self)
    
class HLSLiveHTTPServer(ThreadingHTTPServer):
    
    def __init__(self, server_address, RequestHandlerClass, playlists, window):
        ThreadingHTTPServer.__init__(self, server_address, RequestHandlerClass, True)
        self.playlists = playlists
        self.window = window
        self.starttime = time.time()

class SegmentsDownloader(threading.Thread):
    
    def __init__(self, url, ondiskfilename, maxtime):
        threading.Thread.__init__(self)
        self.url = url
        self.ondiskfilename = ondiskfilename
        self.maxtime = maxtime
        self.segmentsmap = dict()
        self.exit = 0
        self.playlist = None

    def dump(self):
        for i in segmentsmap:
            seg = segmentsmap[i];
            print(seg)
            
    def playlist_duration(self, playlist):
        total = 0
        for seg in playlist.segments:
            total += seg.duration
        return total

    def merge_playlist(self, playlist, newsegments):
        basename = self.ondiskfilename.split('.')[0]
        for i in range(len(playlist.segments)):
            if (self.playlist is not None and
                playlist.media_sequence > self.playlist.media_sequence and
                (playlist.media_sequence - 1) not in self.segmentsmap):
                print("unmatched sequence in update ", playlist.media_sequence,
                      " >> ", self.playlist.media_sequence)
                return
            seg = playlist.segments[i]
            sequence = playlist.media_sequence + i
            try:
                extension = seg.uri.split('.')[-1]
            except:
                extension = 'unk'
            setattr(seg,'ondiskfilename', basename + '_segment_' + str(sequence) + '.' + extension)
            if sequence not in self.segmentsmap:
                self.segmentsmap[sequence] = 1
                if self.playlist is not None:
                    self.playlist.segments.append(seg)
                    newsegments.append(seg)
        if self.playlist is None:
            self.playlist = playlist
            newsegments.extend(playlist.segments)
            if playlist.discontinuity_sequence is None:
                playlist.discontinuity_sequence = 0
            self.playlist.is_endlist = 1

    def retrieve_playlist(self, url):
        handle = urllib.request.urlopen(url)
        content = handle.read()
        return m3u8.loads(content.decode('utf-8'))

    def download_segments(self, segments):
        for seg in segments:
            try:
                uri = seg.absolute_uri
            except:
                uri = urllib.parse.urljoin(self.url, seg.uri)
            filename = getattr(seg, 'ondiskfilename')
            if not os.path.isfile(filename):
                handle = urllib.request.urlopen(uri)
                f = open(filename, "wb")
                f.write(handle.read())
                f.close()
            seg.uri = filename

    def filename(self):
        return self.ondiskfilename

    def save(self, filename):
        if self.playlist is not None:
            # patch segment written urls before writing playlist dump
            for seg in self.playlist.segments:
                seg.uri = getattr(seg, 'ondiskfilename')
            f = open(filename, "wt")
            f.write(self.playlist.dumps())
            f.close()

    def stop(self):
        self.exit = 1

    def run(self):
        while not self.exit:
            try:
                playlist = self.retrieve_playlist(self.url)
            except urllib.error.HTTPError as err:
                print("HTTP Failure downloading playlists, dump will be incomplete:", err.code, err.reason)
                self.exit = 1
                break
            if self.exit:
                break
            newsegmentslist = copy.deepcopy(playlist.segments)
            newsegmentslist.clear()

            duration = self.playlist_duration(playlist)
            self.merge_playlist(playlist, newsegmentslist)
            total_duration = self.playlist_duration(self.playlist)

            print(self.url)
            print("updated, total segments ", len(self.segmentsmap))
            sleepduration = max(duration / 2, playlist.target_duration)
            sleepduration = min(sleepduration, self.maxtime)
            print("next update in " + "{:.2f}".format(sleepduration))
            updatestart = time.time()
            try:
                self.download_segments(newsegmentslist)
            except urllib.error.HTTPError as err:
                print("HTTP Failure downloading segments, dump will be incomplete:", err.code, err.reason)
                self.exit = 1
                break;
            sleepduration -= time.time() - updatestart
            while sleepduration > 0 and not self.exit:
                time.sleep(0.1)
                sleepduration -= 0.1

def parse_main_playlist(url, maxtime):
    todownload = list()
    downloaders = list()
    main = m3u8.load(url)
    if main.is_variant:
        for idx, pl in enumerate(main.media):
            if not pl.absolute_uri:
                continue
            ondiskfilename = 'media_' + str(idx) + '.m3u8'
            todownload.append(tuple((pl.absolute_uri, ondiskfilename)))
            setattr(pl,'ondiskfilename', ondiskfilename)
            pl.uri = ondiskfilename
        for idx, pl in enumerate(main.playlists):
            ondiskfilename = 'playlist_' + str(idx) + '.m3u8'
            todownload.append(tuple((pl.absolute_uri, ondiskfilename)))
            setattr(pl,'ondiskfilename', ondiskfilename)
            pl.uri = ondiskfilename
        f = open("index.m3u8", "wt")
        f.write(main.dumps())
        f.close()
    else:
        todownload.append(tuple((url, "index.m3u8")))

    for urltuple in todownload:
        print("starting: " + urltuple[0])
        dl = SegmentsDownloader(urltuple[0], urltuple[1], maxtime)
        dl.start()
        downloaders.append(dl)

    while maxtime > 0:
        time.sleep(1)
        for dl in downloaders:
            if dl.exit:
                maxtime = 1
                break
        maxtime -= 1

    for dl in downloaders:
        dl.stop()
    for dl in downloaders:
        dl.join()
        dl.save(dl.filename())

def replay(filename, window):
    uris = list()
    main = m3u8.load(filename)
    if main.is_variant:
        for pl in main.playlists:
            uris.append(pl.uri)
        for pl in main.media:
            if not pl.absolute_uri:
                continue
            uris.append(pl.uri)
    else:
        uris.append(filename)
    playlists = dict()
    for uri in uris:
        filename = filename_only(uri)
        print("loading " + filename)
        playlists[filename] = m3u8.load(filename)
    rh = SegmentsServerHandler
    httpd = HLSLiveHTTPServer(('', 8080), rh, playlists, window)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    if len(sys.argv) > 3:
        if sys.argv[1] == "save":
            parse_main_playlist(sys.argv[2], float(sys.argv[3]))
        elif sys.argv[1] == "replay":
            replay(sys.argv[2], float(sys.argv[3]))
    else:
        print("save [url] [duration_in_secs]")
        print("replay [manifest] [livewindow_in_secs]")
